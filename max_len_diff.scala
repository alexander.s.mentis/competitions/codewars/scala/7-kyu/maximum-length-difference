object Kata {

  def mxdiflg(a1: List[String], a2: List[String]): Int = {
    if (a1.isEmpty || a2.isEmpty) -1
    else {
      val a1Sorted = a1.sortBy(_.length)
      val a2Sorted = a2.sortBy(_.length)
      math.abs(a1Sorted.head.length - a2Sorted.last.length) max 
      math.abs(a1Sorted.last.length - a2Sorted.head.length)
    }
  }
}